import { ConfigService } from '@nestjs/config';
import { TYPEORM_CONFIG} from '../config/constats';// nombre de la variable de entrono creada al cargar la configuracion en app.module
import * as fs from 'fs';

/**
 * Generar el archvo ormconfig.json
 */
const generateTypeOrmConfigFile = (configService: ConfigService) => {
    const typeOrmConfig = configService.get(TYPEORM_CONFIG);
    fs.writeFileSync(
        'ormconfig.json',
        JSON.stringify(typeOrmConfig,null,2)
    );
};

export default generateTypeOrmConfigFile
// https://github.com/ruslanguns/nestjs-myblog