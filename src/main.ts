import { Logger,ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config/dist'
import { AppModule } from './app.module';
import generateTypeOrmConfigFile from './scripts/generate-typeormconfg'

async function bootstrap() {
  const logger = new Logger('ApplicationDetails',true);
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);// para usar variables de entorno espesificamente la creada con la configuracion de la basede datos
  const port = configService.get('APP_PORT');
  
  generateTypeOrmConfigFile(configService);// generar el archvo ormconfig.json

  app.setGlobalPrefix('api/v0.2');// para agregar un prefijo global a la app
  app.useGlobalPipes(new ValidationPipe({
      whitelist:true
    })
  );// validar los dto encada end-poin

  await app.listen(port);
  logger.log(`The server is on ${ await app.getUrl() }`);
}
bootstrap();
