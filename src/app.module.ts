import { AuthModule } from './modules/auth/auth.module';
import { RolModule } from './modules/rol/rol.module';
import { UserModule } from './modules/user/user.module';
import { DatabaseModule } from './database/database.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config/dist/config.module';
import databaseConfig from './config/databade.config';

@Module({
  imports: [
    AuthModule,
    RolModule,
    UserModule,
    DatabaseModule,
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos
      isGlobal: true,
      envFilePath: '.env'
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
// crear el directorio config y un archivo constants.ts
//  Para almacenar las variables de configuración en el entorno npm i --save @nestjs/config e importamos ConfigModule
// crear directorio database ademas de modole y service
//  Para la base de datos imstalar typeome y sql npm install --save @nestjs/typeorm typeorm mysql
//  crear neel directorio config un archivo para la configuracion de la base de datos database.config
// crear un directorio scripts y colocal el arcchvo generate-typeormconfg.ts para usar las migraciones
//  colocar en el main la llamada a la funcion del script  generate-typeormconfg
// incluir en package los scripts
//  "migration:create": "typeorm migration:create -n" => npm rum migration:create nameMigration
//  "migration:generate": "typeorm migration:generate -n" => npm rum migration:generate nameMigration
//  "migration:run": "typeorm migration:revert" => npm rum migration:run
//  "migration:revert": "typeorm migration:revert" => npm rum migration:revert
// Para magejar los dto intalar npm i --save class-validator class-transformer
//  usando Auto-validation creamos un ValidationPipe global para la aplicacion en main
// crear directorio modules/user, modules/rol y  modules/auth