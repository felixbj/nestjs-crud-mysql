import { EntityRepository, Repository } from "typeorm"
import { User } from "./user.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User>{}
// clase para aceder a la base de datos 