import { BaseEntity,Column,Entity,JoinColumn,JoinTable,ManyToMany,OneToOne,PrimaryGeneratedColumn } from "typeorm";
import { Rol } from "../../rol/entity";
import { Profile } from "./profile.entity";
import { UserStatus } from "../enum";
import { USER_ACTIVE } from "src/config/constats";

@Entity('users') // DEcorador indicando que es una entidad y elnombtre de la entidad... nombre de la tabla en la bas de datos
export class User extends BaseEntity{
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ type:'varchar',nullable:false,unique:true,length:24 })
    username: string;
    @Column({ type:'varchar',nullable:false,unique:true })
    email: string;
    @Column({ type:'varchar',nullable:false })
    password: string;
    @Column({ type:'varchar',default:USER_ACTIVE,length:50,nullable:false })
    status: UserStatus;
    @OneToOne(() => Profile, profile => profile.user,{
        eager: true, // siempre que se carge el usuario se cargara el prefil
        cascade: true, // al guardar un usuario se guardara un  perfil
        onDelete: 'CASCADE'
    }) // relacion con profile y su inversa de profile a user
    @JoinColumn()
    profile: Profile;
    @ManyToMany(() => Rol, rol => rol.users,{
        eager: true,
    })
    @JoinTable()
    role: Rol[];
    @Column({ type:'text',name:'remember_token',nullable:true })
    rememberToken: string;
    @Column({ type:'boolean',default:true })
    isActive: boolean;
    @Column({ type:'timestamp',name:'created_at',default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
    @Column({ type:'timestamp',name:'updated_at',default: () => 'CURRENT_TIMESTAMP',nullable:true })
    updatedAt: Date;
}