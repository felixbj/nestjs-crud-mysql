import { BaseEntity,Column,Entity,OneToOne,PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity('profiles') // DEcorador indicando que es una entidad y elnombtre de la entidad... nombre de la tabla en la bas de datos
export class Profile extends BaseEntity{
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ type:'varchar',nullable:false,default:'',length:100 })
    firstname: string;
    @Column({ type:'varchar',nullable:true,length:100 })
    lastname: string;
    @Column({ type:'timestamp',default: () => 'CURRENT_TIMESTAMP',nullable:false })
    birthdate: Date;
    @OneToOne(() => User, user => user.profile) // relacion con user y la inversa
    user: User;
    @Column({ type:'timestamp',name:'created_at',default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
    @Column({ type:'timestamp',name:'updated_at',default: () => 'CURRENT_TIMESTAMP',nullable:true })
    updatedAt: Date;
}