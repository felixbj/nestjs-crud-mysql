import { Type } from "class-transformer";
import { IsEmail,IsNotEmpty,IsOptional,Length } from "class-validator";
import { UpdateProfileDto } from "./updateProfile.dto";

export class UpdateUserProfileDto {
    @IsNotEmpty()
    @IsOptional()
    @Length(3,50)
    readonly username: string;
    @IsNotEmpty()
    @IsEmail()
    @IsOptional()
    readonly email: string;
    @IsNotEmpty()
    @IsOptional()
    @Length(8,24)
    readonly password: string;
    @IsNotEmpty()
    @IsOptional()
    readonly isActive: boolean;
    @IsNotEmpty()
    @IsOptional()
    @Type(() => UpdateProfileDto)
    profile : UpdateProfileDto;
}