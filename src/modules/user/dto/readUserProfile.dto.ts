import { IsEmail, IsNumber, IsString } from "class-validator";
import { Exclude,Expose, Type } from "class-transformer"
import { UserStatus } from "../enum";
import { ReadProfileDto } from "./readProfile.dto";

@Exclude()
export class ReadUserProfileDto {
    @Expose()
    @IsNumber()
    readonly id: number;
    @Expose()
    @IsString()
    readonly username: string;
    @Expose()
    @IsString()
    @IsEmail()
    readonly email: string;
    @Expose()
    @IsString()
    readonly status: UserStatus;
    @Expose()
    @Type(() => ReadProfileDto)
    profile : ReadProfileDto;
}