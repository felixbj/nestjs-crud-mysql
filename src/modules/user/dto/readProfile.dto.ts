import { IsNumber,IsString } from "class-validator";
import { Exclude,Expose } from "class-transformer"

@Exclude()
export class ReadProfileDto {
    @Expose()
    @IsNumber()
    readonly id: number;
    @Expose()
    @IsString()
    readonly firstname: string;
    @Expose()
    @IsString()
    readonly lastname: string;
    @Expose()
    @IsString()
    readonly birthdate: string;
}