import { IsEmail,IsNotEmpty,Length } from "class-validator";
import { UserStatus } from "../enum";

export class CreateUserDto {
    @IsNotEmpty()
    @Length(3,50)
    readonly username: string;
    @IsNotEmpty()
    @IsEmail()
    readonly email: string;
    @IsNotEmpty()
    @Length(8,24)
    readonly password: string;
    @IsNotEmpty()
    readonly status: UserStatus;
}