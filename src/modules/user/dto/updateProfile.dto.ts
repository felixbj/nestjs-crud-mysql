import { IsNotEmpty, IsOptional } from "class-validator";

export class UpdateProfileDto {
    @IsNotEmpty()
    @IsOptional()
    readonly name: string;
    @IsNotEmpty()
    @IsOptional()
    readonly lastname: string;
    @IsNotEmpty()
    @IsOptional()
    readonly birthdate: string;
}