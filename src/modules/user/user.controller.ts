import { Body, Controller,Delete,Get,Param,ParseIntPipe,Post,Put,UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { USER_ADMINISTER } from 'src/config/constats';
import { AuthService } from '../auth/auth.service';
import { RolAccess } from '../rol/decorator/role.decorator';
import { RolAccessGuard } from '../rol/guard/rol.guard';
import { CreateUserDto, UpdateUserDto, UpdateUserProfileDto } from './dto';
import { UserService } from './user.service';

@UseGuards(AuthGuard())// protege con la auth toda la clase....
@Controller('user')
export class UserController {
    constructor(
        private readonly userService:UserService,
        private readonly authService:AuthService
    ){}// constructor para incorporar el servicio
    @Get()// cada ruta en el modulo
    @RolAccess(USER_ADMINISTER)// indicar nivel de acceso
    @UseGuards(RolAccessGuard)// chequea si el usuario logeado cumple con el nivel de acceso
    async getAllUser(){
        const data = await this.userService.getAll();//importamte await y async en el metodo
        const token = await this.authService.renewAutoToken();
        return {
            message: 'Todos los usuarios',
            data,
            token
        };
    }
    @Get(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async getOneUser(@Param('id',ParseIntPipe) idUser:number){
        const data = await this.userService.getOne(idUser);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Un solo usuario con el id ${idUser}`,
            data,
            token
        };
    }
    @Post()
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async createOneUser(@Body() userDto:CreateUserDto){
        const data = await this.userService.createOne(userDto);//importamte await y async en el metodo
        const token = await this.authService.renewAutoToken();
        return {
            message: `Crea un solo usuario con el nombre ${data.username}`,
            data,
            token
        };
    }
    @Post('setonrol/:userId/:rolId')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async setOneRolToUser(@Param('userId',ParseIntPipe) userId:number,@Param('rolId',ParseIntPipe) rolId:number){
        const data = await this.userService.setOneRol(userId,rolId);//importamte await y async en el metodo
        const token = await this.authService.renewAutoToken();
        return {
            message: `Agerga un rol un solo usuario con el id ${userId}`,
            data,
            token
        };
    }
    @Put(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async editOneUser(@Param('id',ParseIntPipe) idUser:number,@Body() userDto:UpdateUserDto){
        const data = await this.userService.editOne(idUser,userDto);//importamte await y async en el metodo
        const token = await this.authService.renewAutoToken();
        return {
            message: `Edita un solo usuario con el id ${data.id}`,
            data,
            token
        };
    }
    @Delete(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async markDeleteOne(@Param('id',ParseIntPipe) idUser:number){
        const data =  await this.userService.markDeleteOne(idUser);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Elimina un solo usuario con el id ${idUser}`,
            data,
            token
        };
    }
    @Delete('delete/:id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async deleteOneUser(@Param('id',ParseIntPipe) userId:number){
        const data =  await this.userService.deleteOne(userId);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Elimina un solo usuario con el id ${userId}`,
            data,
            token
        };
    }
    @Delete('deleteonrol/:userId/:rolId')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async deleteOneRolToUser(@Param('userId',ParseIntPipe) userId:number,@Param('rolId',ParseIntPipe) rolId:number){
        const data =  await this.userService.deleteOneRol(userId,rolId);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Elimina un solo usuario con el id ${userId}`,
            data,
            token
        };
    }
    /*
    PROFILE
    */
    @Get('profile/read')
    async getMyUser(){
        const data =  await this.userService.getMy();
        const token = await this.authService.renewAutoToken();
        return {
            message: `Mi usuario`,
            data,
            token
        };
    }
    @Put('profile/update')
    async editMyUser(@Body() profileDto:UpdateUserProfileDto){
        const data =  await this.userService.editMy(profileDto);
        const token = await this.authService.renewAutoToken();
        if(typeof data != 'string'){
            return {
                message: `Editado mi Perifil`,
                data,
                token
            };
        }else{
            return {
                message: `Editado mi usuario, debe iniciar sesion de nuevo`
            };
        }
    }
    @Delete('profile/delete')
    async markDeleteMyUser(){
        const data =  await this.userService.markDeleteMy();
        return {
            message: `Eliminado mi usuario`,
            data
        };
    }
}