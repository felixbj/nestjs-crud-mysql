import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository,ProfileRepository } from './entity'
import { AuthModule } from '../auth/auth.module';
import { RolRepository } from '../rol/entity';

@Module({   
    imports: [
        TypeOrmModule.forFeature([UserRepository,ProfileRepository,RolRepository]),
        AuthModule
    ],
    controllers: [
        UserController,
    ],
    providers: [
        UserService
    ],
})
export class UserModule { }
// crear los directorios dto, enum y entity
//  crear en entity un repositorio y entidad para usuario y perfil
//  importar los repositorios que usaremos en user
// hecha la autenticacion inportamos authModule para usarlo