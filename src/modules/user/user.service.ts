import { BadRequestException,ConflictException,Inject,Injectable,NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Profile,ProfileRepository,User,UserRepository } from './entity';
import { CreateUserDto,ReadUserDto, ReadUserProfileDto, UpdateUserDto, UpdateUserProfileDto } from './dto';
import { Rol,RolRepository } from '../rol/entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { BCRYPT_SALT, DEFAULT_ROL,ROL_ACTIVE,USER_ACTIVE } from 'src/config/constats';
import { REQUEST } from '@nestjs/core';
import { genSalt, hash } from 'bcryptjs';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)// importante incorporar el repo de tpo entdad user
        private readonly userRepository:UserRepository,
        @InjectRepository(Rol)// importante incorporar el repo de tpo entdad rol
        private readonly rolRepository:RolRepository,
        @InjectRepository(Profile)// importante incorporar el repo de tpo entdad rol
        private readonly profileRepository:ProfileRepository,
        @Inject(REQUEST)
        private readonly request
    ){}// constructor para usar el repostorio typeorm y la entidad user
    async getAll():Promise<ReadUserDto[]>{
        return (await this.userRepository.find()).map(
            (user:User) => plainToClass(ReadUserDto,user)// class-transformer pasar la clase user a un dto
        );
    }
    async getOne(id:number):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        const data = await this.userRepository.findOne(id,{ relations: ["profile"] });
        if (!data) throw new NotFoundException()
        return plainToClass(ReadUserDto,data);
    }
    async createOne(user:CreateUserDto):Promise<ReadUserDto>{
        if (!user) throw new BadRequestException();
        const { username,email,password } = user;
        const f = await this.userRepository.findOne({
            where:[{ username },{ email }]
        });
        if(f) throw new ConflictException();
        const data = this.userRepository.create(user);
        data.password = await hash(password,await genSalt(BCRYPT_SALT)); 
        const profileUser = new Profile();// perfil de usuario 
        const defaultRole = await this.rolRepository.findOne({where: {name: DEFAULT_ROL}})
        data.profile = profileUser;// rol de usuario
        data.role = [defaultRole];
        return plainToClass(ReadUserDto, await this.userRepository.save(data));
    }
    async editOne(id:number,user:UpdateUserDto):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        if (!user) throw new BadRequestException();
        const data = await this.userRepository.findOne(id);
        if (!data) throw new NotFoundException()
        const editedData = (!user.hasOwnProperty('password')) ? Object.assign(data,user) : Object.assign(data,user,{ password:await hash(user.password,await genSalt(BCRYPT_SALT)) }) ;
        return plainToClass(ReadUserDto, await this.userRepository.save(editedData));
    }
    async markDeleteOne(id:number):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        const user = await this.userRepository.findOne(id);
        if (!user) throw new NotFoundException()
        const editedUser = Object.assign(user,{ isActive: false });//marca de elimnado
        return plainToClass(ReadUserDto, await this.userRepository.save(editedUser));
    }
    async deleteOne(id:number):Promise<DeleteResult>{
        if (!id) throw new BadRequestException();
        const data = await this.userRepository.findOne(id,{ relations: ["profile"],where: { isActive: true } });
        if (!data) throw new NotFoundException()
        await this.profileRepository.delete(data.profile.id);   
        return await this.userRepository.delete(id);
    }
    /*
    ROLE
    */
    async setOneRol(userId: number, rolId:number):Promise<ReadUserDto>{
        if (!userId && !rolId) throw new BadRequestException();
        const user = await this.userRepository.findOne(userId,{ where: { status: USER_ACTIVE } });
        if(!user) throw new NotFoundException()
        const rol = await this.rolRepository.findOne(rolId,{ where: { status: ROL_ACTIVE } });
        if(!rol) throw new NotFoundException()
        user.role.push(rol);// agrega rol al array de role
        return plainToClass(ReadUserDto, await this.userRepository.save(user));
    }
    async deleteOneRol(userId: number, rolId:number):Promise<ReadUserDto>{
        if (!userId && !rolId) throw new BadRequestException();
        const user = await this.userRepository.findOne(userId,{ where: { status: USER_ACTIVE } });
        if(!user) throw new NotFoundException()
        const rol = await this.rolRepository.findOne(rolId,{ where: { status: ROL_ACTIVE } });
        if(!rol) throw new NotFoundException()
        const i = user.role.map(function(e) { return e.id; }).indexOf(rolId);// obtener index del rol
        i !== -1 && user.role.splice( i, 1 );// splice si es distinto de -1
        return plainToClass(ReadUserDto, await this.userRepository.save(user));
    }
    /*
    PROFILE
    */
    async getMy():Promise<ReadUserProfileDto>{
        const { username,email } = this.request.user
        if (!username && !email) throw new BadRequestException();
        const user = await this.userRepository.findOne({ where:{ username,email },relations:["profile"] });// importante await
        if (!user) throw new NotFoundException()
        return plainToClass(ReadUserProfileDto,user);
    }
    async editMy(profile:UpdateUserProfileDto):Promise<ReadUserProfileDto | String>{
        const { username,email,token } = this.request.user;
        if (!username && !email) throw new BadRequestException();
        const user = await this.userRepository.findOne({ where:{ username,email },relations:["profile"] });// importante await
        if (!user) throw new NotFoundException()

        if (profile.hasOwnProperty('profile')  && Object.entries(profile.profile).length != 0){
            const data = await this.userRepository.findOne(user.profile.id);
            const editedData = Object.assign(data,profile.profile);
            await this.profileRepository.save(editedData);
            delete profile.profile;
        }
        if (!profile.hasOwnProperty('profile')  && Object.entries(profile).length != 0){
            var editedUser;
            if(profile.hasOwnProperty('password')){
                editedUser = Object.assign(user,profile,{ rememberToken: null,password: await hash(profile.password,await genSalt(BCRYPT_SALT)) });
            }else{
                editedUser = Object.assign(user,profile,{ rememberToken: null });
            }
            delete editedUser.profile;
            await this.userRepository.save(editedUser);//guardo el usuaruio y cierro sesion...
            const authorization = await hash(username+email+token,await genSalt(BCRYPT_SALT));
            return authorization;
        }

        return plainToClass(ReadUserProfileDto, await this.userRepository.findOne({ where:{ username,email },relations:["profile"] }));// importante el await
    }
    async markDeleteMy():Promise<ReadUserDto>{
        const { username,email } = this.request.user
        if (!username && !email) throw new BadRequestException();
        const user = await this.userRepository.findOne({ where:{ username,email } });// importante await
        if (!user) throw new NotFoundException()
        const editedUser = Object.assign(user,{ isActive: false });//marca de elimnado
        return plainToClass(ReadUserDto, await this.userRepository.save(editedUser));
    }
}
