import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { DeleteResult } from 'typeorm';
import { CreateRolDto, ReadRolDto, UpdateRolDto } from './dto';
import { Rol,RolRepository } from './entity';

@Injectable()
export class RolService {
    constructor(
        @InjectRepository(Rol)// importante incorporar el repo de tpo entdad rol
        private readonly roleRepository:RolRepository
    ){}
    async getAll():Promise<ReadRolDto[]>{
        return (await this.roleRepository.find()).map(
            (rol:Rol) => plainToClass(ReadRolDto,rol)// class-transformer pasar la clase rol a un dto
        );
    }
    async getOne(id:number):Promise<ReadRolDto>{
        if (!id) throw new BadRequestException();
        const data = await this.roleRepository.findOne(id);
        if (!data) throw new NotFoundException()
        return plainToClass(ReadRolDto,data);
    }
    async createOne(rol:CreateRolDto):Promise<ReadRolDto>{
        const f = await this.roleRepository.findOne({name:rol.name});
        if(f) throw new ConflictException();
        const data = this.roleRepository.create(rol);
        return plainToClass(ReadRolDto,await this.roleRepository.save(data));
    }
    async editOne(id:number,rol:UpdateRolDto):Promise<ReadRolDto>{
        const data = await this.roleRepository.findOne(id);
        if (!data) throw new NotFoundException()
        const editedData = Object.assign(data,rol);//copia las propedades de un objeto a un objeto y lo devuelve
        return plainToClass(ReadRolDto,await this.roleRepository.save(editedData));
    }
    async markDeleteOne(id:number):Promise<ReadRolDto>{
        const data = await this.roleRepository.findOne(id);
        if (!data) throw new NotFoundException()
        data.isActive = false;//marca de elimnado
        return plainToClass(ReadRolDto,await this.roleRepository.save(data));
    }
    async deleteOne(id:number):Promise<DeleteResult>{
        const data = await this.roleRepository.findOne(id);
        if (!data) throw new NotFoundException()
        return await this.roleRepository.delete(id);
    }
}