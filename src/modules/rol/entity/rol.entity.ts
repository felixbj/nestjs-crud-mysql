import { BaseEntity,Column,Entity,ManyToMany,PrimaryGeneratedColumn } from "typeorm";
import { User } from "../../user/entity";
import { RolAccess,RolStatus } from "../enum";
import { ROL_ACTIVE } from "src/config/constats";

@Entity('role') // DEcorador indicando que es una entidad y elnombtre de la entidad... nombre de la tabla en la bas de datos
export class Rol extends BaseEntity{
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ type:'varchar',length:50,unique:true,nullable:false })
    name: string;
    @Column({ type:'varchar',length:50,nullable:false })
    access: RolAccess;
    @Column({ type:'varchar',nullable:false })
    description: string;
    @Column({ type:'varchar',default:ROL_ACTIVE,length:50,nullable:false })
    status: RolStatus;
    @Column({ type:'boolean',default:true })
    isActive: boolean;
    @ManyToMany(() => User, user => user.role)// relacion con user y la inversa
    users: User[];
    @Column({ type:'timestamp',name:'created_at',default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;
    @Column({ type:'timestamp',name:'updated_at',default: () => 'CURRENT_TIMESTAMP',nullable:true })
    updatedAt: Date;
}