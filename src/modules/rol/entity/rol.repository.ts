import { EntityRepository,Repository,getConnection } from "typeorm"
import { Rol } from "./rol.entity";

@EntityRepository(Rol)
export class RolRepository extends Repository<Rol>{}
// clase para aceder a la base de datos 