import { SetMetadata } from '@nestjs/common';

export const RolAccess = (...role:String[]) => SetMetadata(
  'roleaccess',role
);
//@RolAccess() =>> Array de roles admitidos