import { Body,Controller,Delete,Get,Param,ParseIntPipe,Post,Put,UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth/auth.service';
import { RolAccessGuard } from './guard/rol.guard';
import { RolAccess } from './decorator/role.decorator';
import { RolService } from './rol.service';
import { USER_ADMINISTER } from 'src/config/constats';
import { CreateRolDto, UpdateRolDto } from './dto';

@UseGuards(AuthGuard())// protege con la auth y RolAccessGuard toda la clase
@Controller('rol')
export class RolController {
    constructor(
        private readonly rolServce:RolService,
        private readonly authService:AuthService
    ){}// cincorporar el servicios
    @Get()
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async getAllUser(){
        const data = await this.rolServce.getAll();
        const token = await this.authService.renewAutoToken();
        return {
            message: 'Todos los roles',
            data,
            token
        };
    }
    @Get(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async getOneUser(@Param('id',ParseIntPipe) idUser:number){
        const data = await this.rolServce.getOne(idUser);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Un solo rol con el id ${idUser}`,
            data,
            token
        };
    }
    @Post()
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async createOneUser(@Body() userDto:CreateRolDto){
        const data = await this.rolServce.createOne(userDto);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Crea un solo rol con el nombre ${data.name}`,
            data,
            token
        };
    }
    @Put(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async editOneUser(@Param('id',ParseIntPipe) idUser:number,@Body() userDto:UpdateRolDto){
        const data = await this.rolServce.editOne(idUser,userDto);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Edita un solo rol con el id ${data.id}`,
            data,
            token
        };
    }
    @Delete(':id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async markDeleteOne(@Param('id',ParseIntPipe) idUser:number){
        const data =  await this.rolServce.markDeleteOne(idUser);
        const token = await this.authService.renewAutoToken();
        return {
            message: `Elimina un solo rol con el id ${idUser}`,
            data,
            token
        };
    }
    @Delete('delete/:id')
    @RolAccess(USER_ADMINISTER)
    @UseGuards(RolAccessGuard)
    async deleteOneUser(@Param('id',ParseIntPipe) idUser:number){
        const data =  await this.rolServce.deleteOne(idUser);//importamte await y async en el metodo
        const token = await this.authService.renewAutoToken();
        return {
            message: `Elimina un solo rol con el id ${idUser}`,
            data,
            token
        };
    }
}
