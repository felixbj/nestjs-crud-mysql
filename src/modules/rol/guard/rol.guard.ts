
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';// importamos para determinar dependencias en tiempo de ejecucion

@Injectable()
export class RolAccessGuard implements CanActivate {
  constructor(
    private readonly refLector:Reflector,
  ){}
  canActivate(
    context: ExecutionContext,
  ): boolean {
    const roleaccess:string[] = this.refLector.get<string[]>('roleaccess',context.getHandler());
    if(!roleaccess) return false;
    const request = context.switchToHttp().getRequest();
    const { user } = request;// obtenemos el user de auth el payload
    const hasRol = () => user.role.some((role: string) => roleaccess.includes(role));// si tiene el acceso
    return user && user.role && hasRol();
  }
}
// Se lee el arreglo que contiene roleaccess (el decorador creado)
//  si el usuario con el rolapropiado y elusuario existe 
