import { IsNotEmpty } from "class-validator";
import { RolAccess,RolStatus } from "../enum";

export class CreateRolDto {
    @IsNotEmpty()
    readonly name: string;
    @IsNotEmpty()
    readonly description: string;
    @IsNotEmpty()
    readonly access: RolAccess;
    @IsNotEmpty()
    readonly status: RolStatus;
    @IsNotEmpty()
    readonly isActive: boolean;
}