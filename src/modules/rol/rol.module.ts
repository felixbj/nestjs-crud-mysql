import { RolController } from './rol.controller';
import { RolService } from './rol.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolRepository } from '../rol/entity'
import { AuthModule } from '../auth/auth.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([RolRepository]),
        AuthModule
    ],
    controllers: [
        RolController,
    ],
    providers: [
        RolService,
    ],
})
export class RolModule { }
// crear los directorios dto, enum y entity
//  crear en entity un repositorio y entidad para rol
//  importar los repositorios que usaremos en rol
// hecha la autenticacion inportamos authModule para usarlo
// crear un decorador de rol para usar como guard
//  crear un directorio decorators y role.decorator
//  crear guard con cli un guard llamado rol en el directorio guard
