export enum RolStatus{
    'ACTIVE',
    'INACTIVE',
    'LOCKED',
    'REMOVED'
}