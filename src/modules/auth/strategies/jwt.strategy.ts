import { BadRequestException,Injectable,UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from "@nestjs/passport";
import { ConfigService } from "@nestjs/config";
import { JwtService } from '@nestjs/jwt/dist/jwt.service';
import { ExtractJwt,Strategy } from 'passport-jwt';
import { IJwtPayLoad } from '../interface';
import { JWT_SECERT,USER_ACTIVE } from 'src/config/constats';
import { getConnection,IsNull,Not } from 'typeorm';
import { User } from 'src/modules/user/entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
    constructor(
        private readonly configService: ConfigService,
        private readonly jwtService: JwtService,
    ){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get(JWT_SECERT)
            // Configurando la clase padre Jwt y usando como metodo de auturizacion un token por header bearer
        })
    }// Para continuarcon la estratiga para Jwt crear la case validate
    async validate(payload: IJwtPayLoad){
        const { username,email } = payload;
        if (!username && !email) throw new BadRequestException();
        const { rememberToken } =  await this.validateUser(username,email);
        if(!rememberToken)  throw new BadRequestException();
        const isMatch = JSON.stringify(this.jwtService.decode(rememberToken)) === JSON.stringify(payload);
        if(!isMatch) throw new UnauthorizedException();
        return payload;
    }
    /**
     * Comprobar si el existe usuario 
     */
    async validateUser(username: string, email: string): Promise< User> {
        const authRepository = getConnection().getRepository(User);
        const user = await authRepository.findOne({
            where: { username,email,rememberToken:( Not(IsNull()) ),status:USER_ACTIVE }
        });
        if(!user) throw new BadRequestException();
        return user;
    }

}
//clase que va ser un servicio colocando el decorador Injectable