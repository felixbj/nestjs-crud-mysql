import { Body,Controller,Post,Request,UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { SignInDto,SignUpDto } from './dto'

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ){}
    @Post('/signup')
    async signup(@Body() signUpDto: SignUpDto){
        const data = await this.authService.signup(signUpDto);
        return {
            message: `Registro fue exitoso`,
            data
        };
    }
    @Post('/signin')
    async signin(@Body() signUpDto: SignInDto){
        const data = await this.authService.signin(signUpDto);
        return {
            message: `Sesion iniciada con exito`,
            data
        };
    }
    @UseGuards(AuthGuard())// tienes que estar autenticado para poder deslogearte
    @Post('/signout')
    async signout(@Request() req){
        const data = await this.authService.signout(req['user']);
        return {
            message: `Sesion cerrada con exito`,
            data
        };
    }
}