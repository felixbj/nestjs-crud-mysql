import { RolAccess } from '../../rol/enum';

export interface IJwtPayLoad{
    id: number;
    username: string;
    email: string;
    role: RolAccess[];
    token: string;
    iat?: Date;
}
// contrato entre las calse para el comportamiento  tipos 