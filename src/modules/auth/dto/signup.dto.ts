import { IsEmail,IsNotEmpty,IsString,Length } from "class-validator";// para usarlos decoradores de class validator

export class SignUpDto {
    @IsNotEmpty()
    @IsString()
    @Length(3,24)
    readonly username: string;
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    readonly email: string;
    @IsNotEmpty()
    @IsString()
    @Length(8,24)
    readonly password: string;
}