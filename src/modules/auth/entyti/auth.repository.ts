import { BCRYPT_SALT, DEFAULT_ROL } from 'src/config/constats';
import { BadRequestException,NotFoundException } from '@nestjs/common';
import { hash,genSalt} from 'bcryptjs';
import { Rol,RolRepository } from '../../rol/entity';
import { EntityRepository,getConnection,Repository } from 'typeorm';
import { Profile,User } from '../../user/entity';
import { SignUpDto } from '../dto';

@EntityRepository(User)
export class AuthRepository extends Repository<User>{
    /**
     * Crear un nuevo usuario desde Registro
     */
    async signup(signUpDto: SignUpDto): Promise<User>{
        const { username,email,password } = signUpDto;
        if (!username || !email || !password) throw new BadRequestException();
        const profileUser = new Profile();// perfil de usuario 
        const rolRepository: RolRepository = getConnection().getRepository(Rol);// para usar el repositorio sin usar el constructor
        const defaultRole: Rol = await rolRepository.findOne({where: {name: DEFAULT_ROL}});
        if(!defaultRole) throw new NotFoundException();
        const user = new User();// crea ar objeto entidad en memoria
        user.username = username;
        user.email = email;
        user.profile = profileUser;
        user.role = [defaultRole];
        user.rememberToken = null;
        user.password = await hash(password,await genSalt(BCRYPT_SALT)); 
        return await user.save();// sin usar el repositorio solo el objeto entidad user
    }
}
// clase para aceder a la base de datos y metodo para regidtrar un usuario nuevo