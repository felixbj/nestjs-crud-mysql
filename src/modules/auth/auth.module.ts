import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthRepository } from './entyti/auth.repository'
import { PassportModule } from '@nestjs/passport/dist/passport.module';
import { JwtModule } from '@nestjs/jwt/dist/jwt.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy } from './strategies';
import { JWT_SECERT,TOKEN_EXPIRATION } from '../../config/constats';
import { UserRepository } from '../user/entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([AuthRepository,UserRepository]),
        PassportModule.register({
            defaultStrategy: 'jwt'
        }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory(config: ConfigService){
                return{
                    secret: config.get(JWT_SECERT),
                    signOptions: {
                       expiresIn: TOKEN_EXPIRATION
                    }
                }
            }
        })
    ],
    controllers: [
        AuthController,
    ],
    providers: [
        AuthService,
        JwtStrategy,
    ],
    exports:[
        AuthService,
        PassportModule
    ]
})
export class AuthModule { }
// crear los directorios dto, strategies y entity
//  crear en entity un repositorio para auth
//  crear los dto respectivos para signin y signup
//  importar los repositorios que usaremos en auth
// instalar las dependencias para este proceso
//  npm i @nestjs/passport @nestjs/jwt passport passport-jwt bcryptjs 
//  npm i -D @types/bcryptjs @types/passport @types/passport-jwt
//  npm install --save-dev @types/passport-local
//  npm i rand-token
// confifurar jwt y passort
//  importamos y registramos los modulos PassportModule y JwtModule
//  crear una interface para tener un contrato para el payload 
//  creamos en strategies jwt.strategi y lo colocalos en proveiders
//  creada la strategy colocar en los exports junto con passportModule