import { BadRequestException,ConflictException,Inject,Injectable,NotFoundException,UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull,Not,UpdateResult } from 'typeorm';
import { JwtService } from '@nestjs/jwt/dist';
import { plainToClass } from 'class-transformer';
import { AuthRepository } from './entyti'
import { SignUpDto } from './dto';
import { ReadUserDto } from '../user/dto'
import { SignInDto } from '../auth/dto';
import { compare } from 'bcryptjs';
import { User } from '../user/entity';
import { IJwtPayLoad } from './interface';
import { RolAccess } from '../rol/enum';
import * as randtoken from 'rand-token';
import { TOKEN_REFRESH,USER_ACTIVE } from 'src/config/constats';
import { REQUEST } from '@nestjs/core';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(AuthRepository)
        private readonly authRepository: AuthRepository,
        private readonly jwtService: JwtService,
        @Inject(REQUEST)
        private readonly request
    ){}
    /**
     * Crear un nuevo usuario desde Registro
     */
    async signup(singUpDto: SignUpDto): Promise<ReadUserDto>{
        const { username,email } = singUpDto;
        if (!username || !email ) throw new BadRequestException();
        const userExists = await this.authRepository.findOne({
            where:[{ username },{ email }]
        });
        if (userExists) throw new ConflictException();
        const user = await this.authRepository.signup(singUpDto);
        if(!user) throw new NotFoundException();
        return plainToClass(ReadUserDto, user);
    }
    /**
     * Registra usuario en inicio de secion
     */
    async signin(singIpDto: SignInDto): Promise<{token: string}> {
        const { username,password } = singIpDto;
        if (!username && !password) throw new BadRequestException();
        const user = await this.validateUserCredential(username,password);
        const payload: IJwtPayLoad = {
            id: user.id,
            email: user.email,
            username: user.username,
            role: user.role.map(r => r.access as RolAccess),
            token: randtoken.uid(16)
        }
        const token = await this.jwtService.sign(payload);
        await this.authRepository.save({ id:user.id,rememberToken:token });// para la sesion
        return { token };
    }
    /**
     * Saca de registro al usuario se termina la secion
     */
    async signout(reqUser: IJwtPayLoad): Promise<UpdateResult> {
        const { username,email } = reqUser;
        if (!username && !email) throw new BadRequestException();
        const user = await this.validateUser(username,email);
        if(!user) throw new NotFoundException();
        return await this.authRepository.update(user.id,{ rememberToken:null });
    }
    /***********************
     * Servicios utiles
     ***********************/
    /**
     * Renovar el tonek de autorizacion de usuario regresandole uno nuevo
     */
    async renewAutoToken():Promise <String | void> {
        const refreshPayload: IJwtPayLoad = this.request.user;
        if (!refreshPayload) throw new BadRequestException();
        const expToken = ((refreshPayload['exp']) * 1000 - Date.now())/1000;
        if ( Math.sign(expToken) && expToken <= TOKEN_REFRESH ) {
            const user = await this.validateUser(refreshPayload.username,refreshPayload.email);
            if(!user) throw new NotFoundException();
            delete refreshPayload['exp'];
            delete refreshPayload['iat'];
            const token = await this.jwtService.sign(refreshPayload);
            await this.authRepository.save({ id:user.id,rememberToken: token });
            return token;
        }
    }
    /***********************
     * Funciones utiles
     ***********************/
    /**
     * Comprobar si el existe usuario y si sus credenciales son correctas
     */
    async validateUserCredential(username: string, password: string): Promise<User> {
        const user = await this.authRepository.findOne({
            where: { username,isActive:true,status:USER_ACTIVE }
        });
        if(!user) throw new NotFoundException();
        const isMatch = await compare(password,user.password);
        if(!isMatch) throw new UnauthorizedException();
        return user;
    }
    /**
     * Comprobar si el existe usuario 
     */
    async validateUser(username: string, email: string): Promise<User> {
        const user = await this.authRepository.findOne({
            where: { username,email,rememberToken:( Not(IsNull()) ),status:USER_ACTIVE }
        });
        if(!user) throw new NotFoundException();
        return user;
    }
}
