import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { seeder } from "nestjs-seeder";
import { RolRepository } from "./modules/rol/entity";
import { RolSeeder,AdminUserSeeder }from './database/seeders';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TYPEORM_CONFIG } from "./config/constats";
import databaseConfig from './config/databade.config';
import { AuthRepository } from "./modules/auth/entyti";
 
seeder({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos
      isGlobal: true,
      envFilePath: '.env'
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configServce: ConfigService) =>
        configServce.get<TypeOrmModuleOptions>(TYPEORM_CONFIG)
    }),
    TypeOrmModule.forFeature([RolRepository,AuthRepository]),
  ]
}).run([RolSeeder,AdminUserSeeder]);