export const TYPEORM_CONFIG = 'database.config'

export const USER_ADMINISTER = 'ADMINISTER'
export const USER_PUBLISH = 'PUBLISH'
export const USER_VIEWER = 'VIEWER'
export const USER_ACTIVE = 'ACTIVE'
export const USER_INACTIVE = 'INACTIVE'

export const ROL_ACTIVE = 'ACTIVE'
export const ROL_INACTIVE = 'INACTIVE'
export const DEFAULT_ROL = 'VIEWER'
export const ADMIN_ROL = 'ADMINISTER'

export const JWT_SECERT = 'JWTKEY'
export const TOKEN_EXPIRATION = 600
export const TOKEN_REFRESH = 180

export const BCRYPT_SALT = 11