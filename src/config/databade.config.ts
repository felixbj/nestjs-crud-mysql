import { registerAs }from '@nestjs/config';
import { TypeOrmModuleOptions  } from  '@nestjs/typeorm';
import { join } from 'path';

function typeOrmModuleOptons(): TypeOrmModuleOptions {
    return {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: parseInt(process.env.DB_PORT,10),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,

        entities: [join(__dirname,'../modules/**/**/*.entity{.ts,.js}')],
        autoLoadEntities: true,
        
        migrationsRun: true,
        migrationsTableName: "migrations",
        migrations: [join(__dirname,'../migrations/*{.ts,js}')],
        cli:{
            migrationsDir: 'src/migrations',
        },
        
        synchronize: false
    }
}
export default registerAs('database',() =>({
    config: typeOrmModuleOptons()
}));// funcion para regstrar un namespace, congurar una varble de entorno llamada database