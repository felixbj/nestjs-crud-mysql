import { Injectable, NotFoundException } from '@nestjs/common';
import { Seeder } from 'nestjs-seeder';
import { Logger } from '@nestjs/common';
import { Profile, User } from 'src/modules/user/entity';
import { Rol } from 'src/modules/rol/entity';
import { getConnection } from 'typeorm';
import { ADMIN_ROL, BCRYPT_SALT } from 'src/config/constats';
import { genSalt, hash } from 'bcryptjs';
 
@Injectable()
export class AdminUserSeeder implements Seeder {
  async seed(): Promise<any> {
    const logger = new Logger('Seeder',true);
    const rolRepository = getConnection().getRepository(Rol);
    const authRepository = getConnection().getRepository(User);
    if(await authRepository.count() <= 0){
        const profileUser = new Profile(); 
        const admintRole: Rol = await rolRepository.findOne({where: {name: ADMIN_ROL}});
        if(!admintRole) throw new NotFoundException();
        const user = new User();
        user.username = 'administrador';
        user.email = 'administrador@mail.com';
        user.profile = profileUser;
        user.role = [admintRole];
        user.rememberToken = null;
        user.password = await hash('password',await genSalt(BCRYPT_SALT));
        const userAdded = await user.save();
        if(!userAdded)
          logger.log(`Admin failed`);
        else
          logger.log(`Admin added`);      
      return true;
    }
    logger.log('AdminUserSeeder it was already initialized');
    return true;
  }
  async drop(): Promise<any> {
    return false;
  }
}