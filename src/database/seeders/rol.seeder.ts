import { Injectable } from '@nestjs/common';
import { Seeder } from 'nestjs-seeder';
import { Logger } from '@nestjs/common';
import { Rol } from '../../modules/rol/entity';
import { getConnection } from 'typeorm';
import { RolAccess } from '../../modules/rol/enum';

@Injectable()
export class RolSeeder implements Seeder {

  async seed(): Promise<any> {
    const logger = new Logger('Seeder',true);
    const rolRepository = getConnection().getRepository(Rol);
    enum rolAccess {
      'ADMINISTER',
      'PUBLISH',
      'VIEWER'
    };
    if(await rolRepository.count() <= 0){
      for(let rolAccessValue in rolAccess){
        if (isNaN(Number(rolAccessValue))) {
          const rol = Object.assign(new Rol(),{
            name: rolAccessValue,
            description: `${ rolAccessValue } DESCRIPTION`,
            access:  rolAccessValue,
            status: 'ACTIVE',
            isActive: true,
          });
          const rolAdded = await rolRepository.save(rol);
          if(!rolAdded)
            logger.log(`${ rolAccessValue } role failed`);
          else
            logger.log(`${ rolAccessValue } role added`);
        }
      }
      return true;
    }
    logger.log('RolSeeder it was already initialized');
    return true;
  }
  async drop(): Promise<any> {
    return false;
  }
}