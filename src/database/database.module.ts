import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { TYPEORM_CONFIG } from 'src/config/constats';// nombre de la variable de entrono creada al cargar la configuracion en app.module

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
          inject: [ConfigService],
          useFactory: (configServce: ConfigService) =>
            configServce.get<TypeOrmModuleOptions>(TYPEORM_CONFIG)
        })
    ]
})
export class DatabaseModule { }
// Modulo para configuracion de la base de datos